Highcharts.chart('container', {
  chart: {
    type: 'area'
  },
/*  title: {
    text: 'Sources in different time'
  },*/
  title: {
    text: ''
  },
  xAxis: {
    categories: ['BC', '0-1200', '1200-1400', '1400-1600', '1600-1800', '1800-2000'],
    tickmarkPlacement: 'on',
    title: {
      enabled: false
    }
  },
  yAxis: {
    labels: {
      format: '{value}%'
    },
    title: {
      enabled: false
    }
  },
  tooltip: {
    pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.percentage:.1f}%</b> ({point.y:,.0f} )<br/>',
    split: true
  },
  plotOptions: {
    area: {
      stacking: 'percent',
      lineColor: '#ffffff',
      lineWidth: 1,
      marker: {
        lineWidth: 1,
        lineColor: '#ffffff'
      }
    }
  },
  series: [{
    name: 'Original Art Object',
    data: [16, 39, 32, 75, 18, 77],
    color:'#E8DAB2'
  }, {
    name: 'Poem/ Literature',
    data: [1,1,1, 13, 15, 30],
    color:'#DD6E42'
  }, {
    name: 'Other Original Source',
    data: [1,1, 0, 0, 1, 25],
    color:'#4F6D7A'
  }, {
    name: 'Book',
    data: [0, 3, 2, 5, 2, 13],
    color:'#C0D6DF'
  }, {
    name: 'Newspaper',
    data: [0, 0, 0, 0, 3, 21],
    color:'#086788'
  },{
    name: 'Periodical/ Magazine',
    data: [0, 0, 0, 0, 0, 11],
    color:'#8D462A'
  },{
    name: 'Film',
    data: [0, 0, 0, 0, 0, 1],
    color:'#FFBF69'
  },{
    name: 'Media Company',
    data: [0, 0, 0, 0, 0, 2],
    color:'#136F63'
  },{
    name: 'Journal',
    data: [0, 0, 0, 0, 0, 1],
    color:'#6D676E'
  }]
});