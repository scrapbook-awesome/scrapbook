/**
 * SVG path for target icon
 */
var targetSVG = "M9,0C4.029,0,0,4.029,0,9s4.029,9,9,9s9-4.029,9-9S13.971,0,9,0z M9,15.93 c-3.83,0-6.93-3.1-6.93-6.93S5.17,2.07,9,2.07s6.93,3.1,6.93,6.93S12.83,15.93,9,15.93 M12.5,9c0,1.933-1.567,3.5-3.5,3.5S5.5,10.933,5.5,9S7.067,5.5,9,5.5 S12.5,7.067,12.5,9z";

/**
 * SVG path for plane icon
 */
var planeSVG = "m2,106h28l24,30h72l-44,-133h35l80,132h98c21,0 21,34 0,34l-98,0 -80,134h-35l43,-133h-71l-24,30h-28l15,-47";

/**
 * Create the map
 */
var map = AmCharts.makeChart( "chartdiv", {
  "type": "map",
"theme": "dark",

  "projection": "winkel3",
  "dataProvider": {
    "map": "worldLow",

    "lines": [ {
      "id": "line1",
      "arc": -0.85,
      "alpha": 0.2,
      "latitudes": [ 34.7, 40.730610, 46.204391],
      "longitudes": [ 136.5, -73.935242, 6.143158]
    },],
    "images": [ {
      "svgPath": targetSVG,
      "color":"#B52F2F",
      "latitude": 34.7,
      "longitude": 136.5,
      "labelShiftY": 2,
      "zoomLevel": 5,
      "label":"Japan",
  "title": "<b>Dainanagekijo Theater Company</b>, Japan (May-August 2014)",
      "description": "As the artistic and development intern, I worked closely with the Executive Director in composing grant proposals and the company's annual report."
    }, {
      "svgPath": targetSVG,
      "color":"#B52F2F",
      "latitude": 40.730610,
      "longitude": -73.935242,
      "labelShiftY": 2,
      "label":"New York",
      "zoomLevel": 5,
  "title": "<b>Ensemble Studio Theater</b>, New York (May-September 2015)",
      "description": "As the artistic and development intern, I worked closely with the Executive Director in composing grant proposals and the company's annual report."
    }, {
      "svgPath": targetSVG,
      "color":"#B52F2F",
      "latitude": 46.204391,
      "longitude": 6.143158,
      "labelShiftY": 2,
      "label":"Geneva",
      "zoomLevel": 5,
  "title": "<b>World Health Organization, HQ</b> (May-August 2016)",
      "description": "I worked on the WHO initiative to promote local pharmaceuticals manufacturing within the African region to increase access to medicine."
    }, {
      "svgPath": planeSVG,
      "positionOnLine": 0,
      "color": "#000000",
      "animateAlongLine": true,
      "lineId": "line1",
      "flipDirection": false,
      "loop": true,
      "scale": 0.06,
      "positionScale": 1.8
    }]
  },

  "areasSettings": {
    "unlistedAreasColor": "#8dd9ef"
  },

  "imagesSettings": {
    "color": "#585869",
    "rollOverColor": "#585869",
    "selectedColor": "#585869",
    "pauseDuration": 0.2,
    "animationDuration": 4,
    "adjustAnimationSpeed": true
  },

  "linesSettings": {
    "color": "#585869",
    "alpha": 0.4
  },

  "export": {
    "enabled": true
  }

} );